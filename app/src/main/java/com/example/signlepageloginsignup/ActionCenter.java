package com.example.signlepageloginsignup;

import android.content.Context;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;

public class ActionCenter {
    private ArrayList<View> views;
    private ArrayList<View> loginPageElements;
    private ArrayList<View> signUpPageElements;
    private Context context;
    private TextView invoker;
    public enum Direction {LEFT, RIGHT}
    private Point p;

    public ActionCenter(ArrayList<View> views,Point p,Context context,ArrayList<View> el1,ArrayList<View> el2){
        this.views=views;
        this.p=p;
        this.context=context;
        this.loginPageElements=el1;
        this.signUpPageElements=el2;
        invoker=(TextView)views.get(0);
    }

    public void showForms(){
        views.get(0).setOnTouchListener(new View.OnTouchListener() {
            float dX;
            Direction dir = Direction.LEFT;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        dX = v.getX() - event.getRawX();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        actionMove(v, event, dX);
                        break;
                    case MotionEvent.ACTION_UP:
                        dir = actionUp(v, dir);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
    }


    private void actionMove(View view, MotionEvent event, float dX) {
        float xPos=event.getRawX() + dX - ((float) view.getWidth() / 2);
        view.animate().x(xPos).setDuration(0).start();
        views.get(1).animate().x(xPos-views.get(2).getWidth()).setDuration(0).start();
        views.get(2).animate().x(xPos+view.getWidth()).setDuration(0).start();

        for(int i=0;i<loginPageElements.size();i++){
            loginPageElements.get(i).animate().rotation((xPos-views.get(2).getWidth())*-90/views.get(1).getWidth()).setDuration(0).start();
        }
        for(int i=0;i<signUpPageElements.size();i++){
            signUpPageElements.get(i).animate().rotation((xPos*-90)/views.get(2).getWidth()).setDuration(0).start();
        }

    }

    private Direction actionUp(View view, Direction dir) {
        Animation fadeIn= AnimationUtils.loadAnimation(context,R.anim.fade_in);
        Animation fadeOut= AnimationUtils.loadAnimation(context,R.anim.fade_out);
        float xPos,sxPos;
        String strSignUp="sign up";
        String strSignIn="sign in";
        rotate(dir);
        if (dir == Direction.LEFT){
            xPos = 0;
            sxPos=-views.get(1).getWidth();
            dir = Direction.RIGHT;
        }else{
            xPos = p.x - view.getWidth();
            sxPos=0;
            dir = Direction.LEFT;
        }
        view.animate().x(xPos).setDuration(250).start();
        views.get(1).animate().x(sxPos).setDuration(250).start();
        views.get(2).animate().x(xPos+view.getWidth()).setDuration(250).start();
        if(invoker.getText().toString().equals("sign up")) {
            invoker.startAnimation(fadeOut);
            invoker.setBackground(context.getDrawable(R.drawable.invoker_secondary_gradient));
            invoker.startAnimation(fadeIn);
            invoker.setText(strSignIn);
        }
        else if(invoker.getText().toString().equals("sign in")) {
            invoker.startAnimation(fadeOut);
            invoker.setBackground(context.getDrawable(R.drawable.invoker_gradient));
            invoker.startAnimation(fadeIn);
            invoker.setText(strSignUp);
        }
        return dir;
    }

    private void rotate(Direction dir) {
        if (dir == Direction.LEFT) {
            for (int i = 0; i < loginPageElements.size(); i++) {
                loginPageElements.get(i).animate().rotation(90.0f).setDuration(250).start();
            }
            for (int i = 0; i < signUpPageElements.size(); i++) {
                signUpPageElements.get(i).animate().rotation(0.0f).setDuration(250).start();
            }
        }
        if (dir == Direction.RIGHT) {
            for (int i = 0; i < signUpPageElements.size(); i++) {
                signUpPageElements.get(i).animate().rotation(-90.0f).setDuration(250).start();
            }
            for (int i = 0; i < loginPageElements.size(); i++) {
                loginPageElements.get(i).animate().rotation(0.0f).setDuration(250).start();
            }

        }
    }
}
