package com.example.signlepageloginsignup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<View> views =new ArrayList<>();
    private ActionCenter actionCenter;
    private ArrayList<View> loginPageElements=new ArrayList<>();
    private ArrayList<View> singUpPageElements=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        findViewElements();
    }

    private void init() {
        ConstraintLayout singInPage,signUpPage;
        TextView invoker;
        Display display;
        Point p;
        invoker = findViewById(R.id.sign_up_invoker);
        singInPage=findViewById(R.id.sign_in_tab);
        signUpPage=findViewById(R.id.sign_up_tab);
        display = getWindowManager().getDefaultDisplay();
        p = new Point();
        display.getSize(p);
        views.add(invoker);
        views.add(singInPage);
        views.add(signUpPage);
        actionCenter=new ActionCenter(views,p,this.getApplicationContext(),loginPageElements,singUpPageElements);
        showForms();
    }

    private void showForms() {
        actionCenter.showForms();
    }

    private void findViewElements(){
        loginPageElements.add(findViewById(R.id.login_holder));
        loginPageElements.add(findViewById(R.id.login_btn));
        loginPageElements.add(findViewById(R.id.login_assets));

        singUpPageElements.add(findViewById(R.id.linear_layout_in_singup_page));
        singUpPageElements.add(findViewById(R.id.btn_Sign_up));
    }
}
